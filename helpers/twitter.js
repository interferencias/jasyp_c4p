"use strict";

const TWITTER_TWEET_LENGTH = 329;

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

const TWITTER = require('twitter-lite');

const API_TWITTER = new TWITTER(CONFIG.twitter);

module.exports = {
  sendTweet: function(mode, language, type, values, merge_url) {
    let pre_info, post_info, contact, title_max_lenght, title, tweet;

    pre_info = "‼️ [#esLibre2024] 𝗡𝗨𝗘𝗩𝗔 𝗣𝗥𝗢𝗣𝗨𝗘𝗦𝗧𝗔 𝗥𝗘𝗖𝗜𝗕𝗜𝗗𝗔:\n\n";
    post_info = "\n\n¡Mándanos la tuya! 🐧\n\n";
    post_info += "¡Danos tu opinión! 💬 " + merge_url;

    if (values.twitter === "") {
      contact = values.name.length > 30 ? values.name.slice(0, 29) + "." : contact = values.name;
    } else {
      if (values.twitter.charAt(0) != '@') {
        values.twitter = "@" + values.twitter;
      }

      contact = values.twitter;
    }

    title_max_lenght = TWITTER_TWEET_LENGTH - (pre_info.length + contact.length + post_info.length);

    if (type != CONFIG.type.table) {
        if (values.title.length > title_max_lenght) {
          title = values.title.slice(0, title_max_lenght - 3) + "...";
        } else {
          title = values.title;
        }
    }

    tweet = pre_info + contact + " - " + title + post_info;

    console.log('\n[TWITTER API] Tweet sent: "' + tweet + '"\n\n');

    API_TWITTER.post('statuses/update', {
      status: tweet
    }).then(result => {
      console.log('\n[TWITTER API] Tweet sent: "' + result.text + '"');
    }).catch(console.error);
  }
};
