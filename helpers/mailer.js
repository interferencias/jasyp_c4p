"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var nodemailer = require("nodemailer");

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var templates = require(PATH.join(__dirname, "..", "helpers", "templates"));

function configMailer(name, email, subject, html, filename, content) {
  return nodemailer.createTransport({
    host: CONFIG.email.smtp + "." + CONFIG.email.server,
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
      user: CONFIG.email.user,
      pass: CONFIG.email.password
    }
  }, {
    from: CONFIG.email.name + " <" + CONFIG.email.user + ">",
    replyTo: CONFIG.email.name + " <" + CONFIG.email.replyto + ">",
    bcc: CONFIG.email.bcc,
    to: name.concat(" <", email, ">"),
    organization: CONFIG.organization,
    subject: subject,
    html: html,
    attachments: [{
      filename: filename,
      content: content
    }]
  });
};

function configMailerGrants(name, email, subject, html) {
  return nodemailer.createTransport({
    host: CONFIG.email.smtp + "." + CONFIG.email.server,
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
      user: CONFIG.email.user,
      pass: CONFIG.email.password
    }
  }, {
    from: CONFIG.email.name + " <" + CONFIG.email.user + ">",
    replyTo: CONFIG.email.name + " <" + CONFIG.email.replyto + ">",
    bcc: CONFIG.email.bcc,
    to: name.concat(" <", email, ">"),
    organization: CONFIG.organization,
    subject: subject,
    html: html
  });
};

function getMailerGrants(values) {
  let type_name, addressee, subject, html;

  type_name = "Petición recibida";
  addressee = values.name;
  subject = "[" + CONFIG.event + " " + CONFIG.year + "] " + type_name;
  html = templates.getMailGrants(values);

  return configMailerGrants(addressee, values.email, subject, html);
}

function getMailerCaring(values) {
  let type_name, addressee, subject, html;

  type_name = "Petición recibida";
  addressee = values.name;
  subject = "[" + CONFIG.event + " " + CONFIG.year + "] " + type_name;
  html = templates.getMailCaring(values);

  return configMailerGrants(addressee, values.email, subject, html);
}

function getMailer(mode, language, type, values, merge_url) {
  let type_name, html, filename, content, subject, addressee;

  if (language == CONFIG.language.spanish) {
    type_name = "Propuesta recibida";
  } else if (language == CONFIG.language.english) {
    type_name = "Proposal received";
  } else {
    type_name = "Proposta recibida";
  }

  if (type == CONFIG.type.venue || type == CONFIG.type.table) {
    filename = slugify.getFileName(values.name) + ".md";
  } else {
    filename = slugify.getFileName(values.title) + ".md";
  }

  addressee = values.name;
  subject = "[" + CONFIG.event + " " + CONFIG.year + "] " + type_name;

  switch (type) {
    case CONFIG.type.proposal:
    case CONFIG.type.talk:
    case CONFIG.type.workshop:
      html = templates.getMailProposals(language, values, merge_url);
      content = templates.getTemplateProposals(values);
      break;
    case CONFIG.type.devroom:
      html = templates.getMailDevrooms(language, values, merge_url);
      content = templates.getTemplateDevrooms(values);
      break;
    case CONFIG.type.table:
      html = templates.getMailTables(language, values, merge_url);
      content = templates.getTemplateTables(values);
      break;
    case CONFIG.type.venue:
      html = templates.getMailVenues(values, merge_url);
      content = templates.getTemplateVenues(values);
      break;
    case CONFIG.type.misc:
      html = templates.getMailMiscs(language, values, merge_url);
      content = templates.getTemplateMiscs(values);
      break;
    default:
      handlers.errorHandlerCreate(mode, language, res, new Error('[ERROR]: Type of proposal unknown.'));
  }

  return configMailer(addressee, values.email, subject, html, filename, content);
}

module.exports = {
  getMailer,
  getMailerGrants,
  getMailerCaring,
};
