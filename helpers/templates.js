"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var fs = require("fs");
var path = require("path");

function getMailArts(values, merge_url) {
    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, "art.html"), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _title: values.title,
        _abstract: values.abstract,
        _url: values.url,
        _name: values.name,
        _bio: values.bio,
        _web: values.web,
        _mastodon: values.mastodon,
        _twitter: values.twitter,
        _gitlab: values.gitlab,
        _comments: values.comments,
    };

    html = html.replace(/_merge_url|_title|_abstract|_url|_name|_bio|_web|_mastodon|_twitter|_gitlab|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailDevrooms(language, values, merge_url) {
    let filename;

    switch (language) {
        case CONFIG.language.english:
            filename = "devroom-en.html";
            break;
        case CONFIG.language.galician:
            filename = "devroom-gl.html";
            break;
        default:
            filename = "devroom-es.html";
    }

    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, filename), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _title: values.title,
        _description: values.description,
        _name: values.name,
        _community_description: values.community_description,
        _web: values.web,
        _format: values.format,
        _target: values.target,
        _mastodon: values.mastodon,
        _twitter: values.twitter,
        _gitlab: values.gitlab,
        _contact: values.contact,
        _comments: values.comments,
    };

    html = html.replace(/_merge_url|_title|_description|_name|_community_description|_web|_format|_target|_mastodon|_twitter|_gitlab|_contact|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailTables(language, values, merge_url) {
    let filename;

    if (language == CONFIG.language.spanish) {
        filename = "table-es.html";
    } else {
        filename = "table-en.html";
    }

    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, filename), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _name: values.name,
        _web: values.web,
        _community_description: values.community_description,
        _mastodon: values.mastodon,
        _twitter: values.twitter,
        _gitlab: values.gitlab,
        _contact: values.contact,
        _comments: values.comments,
    };

    html = html.replace(/_merge_url|_name|_web|_community_description|_mastodon|_twitter|_gitlab|_contact|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailMiscs(language, values, merge_url) {
    let filename, formats_name;

    switch (language) {
        case CONFIG.language.english:
            filename = "misc-en.html";
            formats_name = {
                article: "Article",
                poster: "Poster",
                project: "Project board",
                other: "Other",
            };
            break;
        case CONFIG.language.galician:
            filename = "misc-gl.html";
            formats_name = {
                article: "Artigos",
                poster: "Pósters",
                project: "Taboleiro de proxectos",
                other: "Outros",
            };
            break;
        default:
            filename = "misc-es.html";
            formats_name = {
                article: "Artículo",
                poster: "Póster",
                project: "Tablón de proyectos",
                other: "Otros",
            };
    }

    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, filename), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _title: values.title,
        _abstract: values.abstract,
        _description: values.description,
        _project: values.project,
        _target: values.target,
        _name: values.name,
        _bio: values.bio,
        _remote: values.remote,
        _web: values.web,
        _mastodon: values.mastodon,
        _twitter: values.twitter,
        _gitlab: values.gitlab,
        _comments: values.comments,
    };

    switch (values.format) {
        case "A":
            html = html.replace(/_format/gi, formats_name.article);
            break;
        case "P":
            html = html.replace(/_format/gi, formats_name.poster);
            break;
        case "T":
            html = html.replace(/_format/gi, formats_name.project);
            break;
        default:
            html = html.replace(/_format/gi, formats_name.other);
    }

    html = html.replace(/_merge_url|_title|_abstract|_description|_project|_target|_name|_bio|_remote|_web|_mastodon|_twitter|_gitlab|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailPosters(values, merge_url) {
    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, "poster.html"), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _title: values.title,
        _abstract: values.abstract,
        _url: values.url,
        _project: values.project,
        _name: values.name,
        _bio: values.bio,
        _web: values.web,
        _mastodon: values.mastodon,
        _twitter: values.twitter,
        _gitlab: values.gitlab,
        _comments: values.comments,
    };

    html = html.replace(/_merge_url|_title|_abstract|_url|_project|_name|_bio|_web|_mastodon|_twitter|_gitlab|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailProposals(language, values, merge_url) {
    let filename, formats_name, accommodation;

    switch (language) {
        case CONFIG.language.english:
            filename = "proposal-en.html";
            formats_name = {
                short_talk: "Short talk",
                long_talk: "Long talk",
                workshop: "Workshop",
                misc: "Miscellaneous",
            };
            accommodation = {
              no_acc: "No application",
              acc_under: "Application for women and under-represented groups of people",
              acc_dis: "Application for persons with disabilities, neurodivergent or similar conditions",
              acc_general: "General application for economically vulnerable individuals",
            };
            break;
        case CONFIG.language.galician:
            filename = "proposal-gl.html";
            formats_name = {
                short_talk: "Relatorio curta",
                long_talk: "Relatorio longa",
                workshop: "Obradoiro",
                misc: "Miscelánea",
            };
            accommodation = {
              no_acc: "Non solicito aloxamento",
              acc_under: "Solicitude de aloxamento para mulleres e grupos de persoas infrarrepresentados",
              acc_dis: "Solicitude de aloxamento para persoas con discapacidade, neurodiverxencia ou similar",
              acc_general: "Solicitude de aloxamento xeral para persoas en situación de vulnerabilidade económica",
            };
            break;
        default:
            filename = "proposal-es.html";
            formats_name = {
                short_talk: "Charla corta",
                long_talk: "Charla larga",
                workshop: "Taller",
                misc: "Miscelánea",
            };
            accommodation = {
              no_acc: "Sin solicitud",
              acc_under: " Solicitud para mujeres y grupos de personas menos representados",
              acc_dis: "Solicitud para personas en situación de discapacidad, neurodivergencia o similar",
              acc_general: "Solicitud general para personas en situación de vulnerabilidad económica",
            };
    }

    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, filename), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _title: values.title,
        _abstract: values.abstract,
        _description: values.description,
        _project: values.project,
        _target: values.target,
        _name: values.name,
        _bio: values.bio,
        _remote: values.remote,
        _web: values.web,
        _mastodon: values.mastodon,
        _twitter: values.twitter,
        _gitlab: values.gitlab,
        _comments: values.comments,
    };

    switch (values.format) {
        case "S":
            html = html.replace(/_format/gi, formats_name.short_talk);
            break;
        case "L":
            html = html.replace(/_format/gi, formats_name.long_talk);
            break;
        case "W":
            html = html.replace(/_format/gi, formats_name.workshop);
            break;
        default:
            html = html.replace(/_format/gi, formats_name.misc);
    }

    switch (values.language) {
        case "S":
            html = html.replace(/_language/gi, "Español");
            break;
        case "C":
            html = html.replace(/_language/gi, "Català");
            break;
        case "G":
            html = html.replace(/_language/gi, "Galego");
            break;
        case "V":
            html = html.replace(/_language/gi, "Valencià");
            break;
        case "B":
            html = html.replace(/_language/gi, "Euskara");
            break;
        case "A":
            html = html.replace(/_language/gi, "Aranès");
            break;
        case "O":
            html = html.replace(/_language/gi, "Otro");
            break;
        default:
            html = html.replace(/_language/gi, "English");
    }

    switch (values.grants) {
        case "M":
            html = html.replace(/_accommodation/gi, accommodation.acc_under);
            break;
        case "E":
            html = html.replace(/_accommodation/gi, accommodation.acc_dis);
            break;
        case "G":
            html = html.replace(/_accommodation/gi, accommodation.acc_general);
            break;
        default:
            html = html.replace(/_accommodation/gi, accommodation.no_acc);
    }

    if (values.remote_req) {
        if (language == CONFIG.language.english) {
            html = html.replace(/_remote_req/gi, "Remote");
        } else {
            html = html.replace(/_remote_req/gi, "Remoto");
        }
    } else {
        if (language == CONFIG.language.english) {
            html = html.replace(/_remote_req/gi, "Face-to-face");
        } else {
            html = html.replace(/_remote_req/gi, "Presencial");
        }
    }

    html = html.replace(/_merge_url|_title|_abstract|_description|_project|_target|_name|_bio|_remote|_web|_mastodon|_twitter|_gitlab|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailVenues(values, merge_url) {
    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, "venue.html"), "utf8");

    const replaces = {
        _merge_url: merge_url,
        _name: values.name,
        _team: values.team,
        _format: values.format,
        _diversity: values.diversity,
        _dates: values.dates,
        _economic: values.economic,
        _facilities: values.facilities,
        _space: values.space,
        _comments: values.comments,
    };

    html = html.replace(/_merge_url|_name|_team|_format|_diversity|_dates|_economic|_facilities|_space|_comments/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailGrants(values) {
    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, "grants-attendance.html"), "utf8");

    const replaces = {
        _name: values.name,
        _bio: values.bio,
    };

    html = html.replace(/_name|_bio/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getMailCaring(values) {
    let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", CONFIG.event, "care-service.html"), "utf8");

    const replaces = {
        _name: values.name,
        _age: values.age,
        _hours: values.hours,
    };

    html = html.replace(/_name|_age|_hours/gi, function(matched) {
        return replaces[matched];
    });

    return html;
}

function getTemplateArts(values) {
    let web_a = "-   Web personal:\n";
    let web_b = "-   Web personal: <" + values.web + ">\n";
    let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
    let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
    let twitter_a = "-   Twitter:\n";
    let twitter_b = "-   Twitter: <https://twitter.com/" + values.twitter + ">\n";
    let gitlab_a = "-   GitLab (u otra forja) o portfolio general:\n";
    let gitlab_b = "-   GitLab (u otra forja) o portfolio general: <" + values.gitlab + ">\n";
    let accept_coc_txt = "-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en las jornadas\n";

    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: arts\n" +
        "author: " + values.name + "\n" +
        "title: " + values.title + "\n" +
        "---\n\n";

    template += "# " + values.title + "\n\n";

    template += values.abstract + "\n\n";

    template += "Enlace a la obra en Wikimedia Commons: <" + values.url + ">\n\n";

    template += "## Autor/a:\n\n";

    template += "-   Nombre: " + values.name + "\n";

    template += "-   Bio: " + values.bio + "\n\n";

    template += "### Info personal:\n\n";

    template += values.web === "" ? web_a : web_b;

    template += values.mastodon === "" ? mastodon_a : mastodon_b;

    template += values.twitter === "" ? twitter_a : twitter_b;

    template += values.gitlab === "" ? gitlab_a : gitlab_b;

    template += "\n## Comentarios\n\n" + values.comments + "\n\n";

    template += "## Condiciones aceptadas\n\n" + accept_coc_txt + "\n";

    return template;
}

function getTemplateDevrooms(values) {
    let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
    let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
    let twitter_a = "-   Twitter:\n";
    let twitter_b = "-   Twitter: <https://twitter.com/" + values.twitter + ">\n";
    let gitlab_a = "-   GitLab (u otra forja) o portfolio general:\n";
    let gitlab_b = "-   GitLab (u otra forja) o portfolio general: <" + values.gitlab + ">\n";
    let accept_coc_txt = "-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso\n";

    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: devrooms\n" +
        "author: " + values.name + "\n" +
        "title: " + values.title + "\n" +
        "---\n\n";

    template += "# " + values.title + "\n\n";

    template += "## Detalles de la propuesta:\n\n";
    template += "-   Descripción: " + values.description + "\n";
    template += "-   Formato: " + values.format + "\n";
    template += "-   Público objetivo: " + values.target + "\n\n";

    template += "## Comunidad que propone la sala:\n\n";
    template += "-   Nombre: " + values.name + "\n";
    template += "-   Info: " + values.community_description + "\n";
    template += "-   Web: <" + values.web + ">\n";
    template += values.mastodon === "" ? mastodon_a : mastodon_b;
    template += values.twitter === "" ? twitter_a : twitter_b;
    template += values.gitlab === "" ? gitlab_a : gitlab_b;

    template += "\n## Comentarios\n\n" + values.comments + "\n\n";

    template += "## Condiciones aceptadas\n\n" + accept_coc_txt + "\n";

    return template;
}

function getTemplateTables(values) {
    let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
    let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
    let twitter_a = "-   Twitter:\n";
    let twitter_b = "-   Twitter: <https://twitter.com/" + values.twitter + ">\n";
    let gitlab_a = "-   GitLab (u otra forja) o portfolio general:\n";
    let gitlab_b = "-   GitLab (u otra forja) o portfolio general: <" + values.gitlab + ">\n";
    let accept_coc_txt = "-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso\n";

    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: tables\n" +
        "author: " + values.name + "\n" +
        "title: " + values.name + "\n" +
        "---\n\n";

    template += "## Comunidad que solicita la mesa:\n\n";
    template += "-   Nombre: " + values.name + "\n";
    template += "-   Web: <" + values.web + ">\n";
    template += "-   Info: " + values.community_description + "\n";
    template += values.mastodon === "" ? mastodon_a : mastodon_b;
    template += values.twitter === "" ? twitter_a : twitter_b;
    template += values.gitlab === "" ? gitlab_a : gitlab_b;

    template += "\n## Comentarios\n\n" + values.comments + "\n\n";

    template += "## Condiciones aceptadas\n\n" + accept_coc_txt + "\n";

    return template;
}

function getTemplateMiscs(values) {
    let project_a = "-   Web del proyecto:\n";
    let project_b = "-   Web del proyecto: <" + values.project + ">\n";
    let web_a = "-   Web personal:\n";
    let web_b = "-   Web personal: <" + values.web + ">\n";
    let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
    let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
    let twitter_a = "-   Twitter:\n";
    let twitter_b = "-   Twitter: <https://twitter.com/" + values.twitter + ">\n";
    let gitlab_a = "-   GitLab (u otra forja) o portfolio general:\n";
    let gitlab_b = "-   GitLab (u otra forja) o portfolio general: <" + values.gitlab + ">\n";
    let accept_coc_txt = "-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso\n";

    let format;
    switch (values.format) {
        case "A":
            format = "Artículo\n";
            break;
        case "P":
            format = "Póster\n";
            break;
        case "T":
            format = "Tablón de proyectos\n";
            break;
        default:
            format = "Otros\n";
    }

    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: misc\n" +
        "author: " + values.name + "\n" +
        "title: " + values.title + "\n" +
        "---\n\n";

    template += "# " + values.title + "\n\n";

    template += values.abstract + "\n\n";

    template += "## Detalles de la propuesta:\n\n";

    template += "-   Tipo de propuesta: " + format;

    template += "-   Descripción: " + values.description + "\n";

    template += values.project === "" ? project_a : project_b;

    template += "-   Público objetivo: " + values.target + "\n\n";

    template += "## Ponente:\n\n";

    template += "-   Nombre: " + values.name + "\n";

    template += "-   Bio: " + values.bio + "\n\n";

    template += "### Info personal:\n\n";

    template += values.web === "" ? web_a : web_b;

    template += values.mastodon === "" ? mastodon_a : mastodon_b;

    template += values.twitter === "" ? twitter_a : twitter_b;

    template += values.gitlab === "" ? gitlab_a : gitlab_b;

    template += "\n## Comentarios\n\n" + values.comments + "\n\n";

    template += "## Condiciones aceptadas\n\n" + accept_coc_txt + "\n";

    return template;
}

function getTemplatePosters(values) {
    let project_a = "-   Web del proyecto:\n\n";
    let project_b = "-   Web del proyecto: <" + values.project + ">\n\n";
    let web_a = "-   Web personal:\n";
    let web_b = "-   Web personal: <" + values.web + ">\n";
    let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
    let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
    let twitter_a = "-   Twitter:\n";
    let twitter_b = "-   Twitter: <https://twitter.com/" + values.twitter + ">\n";
    let gitlab_a = "-   GitLab (u otra forja) o portfolio general:\n";
    let gitlab_b = "-   GitLab (u otra forja) o portfolio general: <" + values.gitlab + ">\n";
    let accept_coc_txt = "-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en las jornadas\n";

    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: posters\n" +
        "author: " + values.name + "\n" +
        "title: " + values.title + "\n" +
        "---\n\n";

    template += "# " + values.title + "\n\n";

    template += values.abstract + "\n\n";

    template += "## Proyecto:\n\n";

    template += "-   Enlace al póster en Wikimedia Commons: <" + values.url + ">\n";

    template += values.project === "" ? project_a : project_b;

    template += "## Autor/a:\n\n";

    template += "-   Nombre: " + values.name + "\n";

    template += "-   Bio: " + values.bio + "\n\n";

    template += "### Info personal:\n\n";

    template += values.web === "" ? web_a : web_b;

    template += values.mastodon === "" ? mastodon_a : mastodon_b;

    template += values.twitter === "" ? twitter_a : twitter_b;

    template += values.gitlab === "" ? gitlab_a : gitlab_b;

    template += "\n## Comentarios\n\n" + values.comments + "\n\n";

    template += "## Condiciones aceptadas\n\n" + accept_coc_txt + "\n";

    return template;
}

function getTemplateProposals(values) {
    let project_a = "-   Web del proyecto:\n";
    let project_b = "-   Web del proyecto: <" + values.project + ">\n";
    let web_a = "-   Web personal:\n";
    let web_b = "-   Web personal: <" + values.web + ">\n";
    let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
    let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
    let twitter_a = "-   Twitter:\n";
    let twitter_b = "-   Twitter: <https://twitter.com/" + values.twitter + ">\n";
    let gitlab_a = "-   GitLab (u otra forja) o portfolio general:\n";
    let gitlab_b = "-   GitLab (u otra forja) o portfolio general: <" + values.gitlab + ">\n";
    let accept_coc_txt = "-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso\n";

    let format, language, category;
    switch (values.format) {
        case "S":
            format = "Charla corta";
            category = "talks";
            break;
        case "L":
            format = "Charla larga";
            category = "talks";
            break;
        case "W":
            format = "Taller";
            category = "workshops";
            break;
        default:
            format = "Otra";
            category = "miscs";
    }

    switch (values.language) {
        case "S":
            language = "Español";
            break;
        case "C":
            language = "Català";
            break;
        case "G":
            language = "Galego";
            break;
        case "V":
            language = "Valencià";
            break;
        case "B":
            language = "Euskara";
            break;
        case "A":
            language = "Aranès";
            break;
        case "O":
            language = "Otro";
            break;
        default:
            language = "English";
    }

    if (values.remote_req) {
        format += " / Remoto\n";
    } else {
        format += " / Presencial\n";
    }

    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: " + category + "\n" +
        "author: " + values.name + "\n" +
        "title: " + values.title + "\n" +
        "---\n\n";

    template += "# " + values.title + "\n\n";

    template += values.abstract + "\n\n";

    template += "## Detalles de la propuesta:\n\n";

    template += "-   Tipo de propuesta: " + format;

    template += "-   Idioma: " + language + "\n";

    template += "-   Descripción: " + values.description + "\n";

    template += values.project === "" ? project_a : project_b;

    template += "-   Público objetivo: " + values.target + "\n\n";

    template += "## Ponente:\n\n";

    template += "-   Nombre: " + values.name + "\n";

    template += "-   Bio: " + values.bio + "\n\n";

    template += "### Info personal:\n\n";

    template += values.web === "" ? web_a : web_b;

    template += values.mastodon === "" ? mastodon_a : mastodon_b;

    template += values.twitter === "" ? twitter_a : twitter_b;

    template += values.gitlab === "" ? gitlab_a : gitlab_b;

    template += "\n## Comentarios\n\n" + values.comments + "\n\n";

    template += "## Condiciones aceptadas\n\n" + accept_coc_txt + "\n";

    return template;
}

function getTemplateVenues(values) {
    let template = "---\n" +
        "layout: 2024/post\n" +
        "section: proposals\n" +
        "category: venues\n" +
        "title: " + values.name + "\n" +
        "---\n\n";

    template += "*Nombre del equipo*: " + values.name + "\n\n";

    template += "## Equipo organizador local:\n\n" + values.team + "\n\n";

    template += "## Formato del congreso:\n\n" + values.format + "\n\n";

    template += "## Acciones para mejorar la diversidad del congreso:\n\n" + values.diversity + "\n\n";

    template += "## Posibles fechas:\n\n" + values.dates + "\n\n";

    template += "## Aspectos económicos:\n\n" + values.economic + "\n\n";

    template += "## Facilidades disponibles:\n\n" + values.facilities + "\n\n";

    template += "## Planificación y uso del espacio físico:\n\n" + values.space + "\n\n";

    template += "## Comentarios:\n\n" + values.comments + "\n\n";

    return template;
}

module.exports = {
    /* Mails content */
    getMailArts,
    getMailDevrooms,
    getMailTables,
    getMailMiscs,
    getMailPosters,
    getMailProposals,
    getMailVenues,
    getMailGrants,
    getMailCaring,
    /* Markdown templates*/
    getTemplateArts,
    getTemplateDevrooms,
    getTemplateTables,
    getTemplateMiscs,
    getTemplatePosters,
    getTemplateProposals,
    getTemplateVenues,
};
