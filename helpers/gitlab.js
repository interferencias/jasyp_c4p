"use strict";

const GITLAB_COMMIT_LENGTH = 72;

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

const UTIL = require('util');
const {
  Gitlab
} = require('@gitbeaker/node');

const API_GITLAB = new Gitlab(CONFIG.gitlab);

var templates = require(PATH.join(__dirname, "..", "helpers", "templates"));
var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var twitter = require(PATH.join(__dirname, "..", "helpers", "twitter"));
var mailer = require(PATH.join(__dirname, "..", "helpers", "mailer"));

module.exports = {
  apiActions: async function(mode, language, type, branch, file, values, res) {
    let type_name, folder, id, commit_text, merge_text, content;

    switch (type) {
      case CONFIG.type.venue:
        type_name = "sedes";
        content = templates.getTemplateVenues(values);
        break;
      case CONFIG.type.talk:
        type_name = "charlas";
        content = templates.getTemplateProposals(values);
        break;
      case CONFIG.type.workshop:
        type_name = "talleres";
        content = templates.getTemplateProposals(values);
        break;
      case CONFIG.type.devroom:
        type_name = "salas";
        content = templates.getTemplateDevrooms(values);
        break;
      case CONFIG.type.table:
        type_name = "mesas";
        content = templates.getTemplateTables(values);
        break;
      case CONFIG.type.misc:
        type_name = "miscelanea";
        content = templates.getTemplateMiscs(values);
        break;
      default:
        handlers.errorHandlerCreate(mode, language, res, new Error('[ERROR]: Type of proposal unknown.'));
    }

    if (type == CONFIG.type.venue) {
      folder = "propuestas/" + CONFIG.year + "/";
      id = values.name;
    } else {
      folder = CONFIG.year + "/" + type_name + "/";

      if (type == CONFIG.type.table){
          id = values.name;
      } else {
          id = values.name + " - " + values.title;
      }
    }


    commit_text = "Añade " + type_name + ": " + id;
    merge_text = "[" + CONFIG.event + CONFIG.year + "/" + type_name.toUpperCase() + "]: " + id;

    await API_GITLAB.Branches.create(process.env.GITLAB_PROJECT, branch, "main")
      .then((branch) => {
        console.log("\n[GITLAB API] Create branch response:" + UTIL.inspect(branch, false, null, true) + "\n");
      }).then(function() {
        API_GITLAB.RepositoryFiles.create(process.env.GITLAB_PROJECT, folder + file, branch, content, commit_text).then((file) => {
          console.log("\n[GITLAB API] Create proposal file response:" + UTIL.inspect(file, false, null, true) + "\n");
        }).catch(error => {
          handlers.errorHandlerCreate(mode, language, res, error);
        });
      }).then(function() {
        API_GITLAB.MergeRequests.create(process.env.GITLAB_PROJECT, branch, "main", merge_text).then((merge) => {
          console.log("\n[GITLAB API] Create merge request response:" + UTIL.inspect(merge, false, null, true) + "\n");

          /*
          if (type != CONFIG.type.venue) {
            twitter.sendTweet(mode, language, type, values, merge.web_url);
          }
          */

          mailer.getMailer(mode, language, type, values, merge.web_url).sendMail({}, function(error, info) {
            if (error) {
              handlers.errorHandlerCreate(mode, language, res, error);
            } else {
              if (mode === CONFIG.mode.debug) {
                res.redirect("/admin/");
              } else {
                switch (language) {
                  case CONFIG.language.galician:
                    res.redirect(CONFIG.external_url_ok_gl);
                    break;
                  case CONFIG.language.english:
                    res.redirect(CONFIG.external_url_ok_en);
                    break;
                  default:
                    res.redirect(CONFIG.external_url_ok_es);
                }
              }
            }
          });
        }).catch(error => {
          handlers.errorHandlerCreate(mode, language, res, error);
        });
      }).catch(error => {
        handlers.errorHandlerCreate(mode, language, res, error);
      });
  }
};
