"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var messages = require(PATH.join(__dirname, "..", "config", "messages"));

function renderView(res, view, title) {
  res.render(view, {
    app_name: CONFIG.app_name,
    title: CONFIG.event_name + title,
    elements: messages.elements
  });
}

function errorHandlerCreate(mode, language, res, error) {
  if (mode === CONFIG.mode.debug) {
    console.log("\n\n[ERROR]:")
    console.log(error);

    res.render("error", {
      app_name: CONFIG.app_name,
      title: CONFIG.event_name + " - Error",
      message: error.message,
      error: error
    });
  } else {
    switch (language) {
      case CONFIG.language.galician:
        res.redirect(CONFIG.external_url_error_gl);
        break;
      case CONFIG.language.english:
        res.redirect(CONFIG.external_url_error_en);
        break;
      default:
        res.redirect(CONFIG.external_url_error_es);
    }
  }

}

module.exports = {
  renderView,
  errorHandlerCreate
};
