"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var fs = require("fs");
var multer = require("multer");

module.exports = multer({
  storage: multer.diskStorage({
    destination: (req, file, callback) => {
      var path = "public/uploads/";
      fs.mkdirsSync(path);
      callback(null, path);
    }
  }),
  limits: {
    fileSize: CONFIG.max_size
  },
  fileFilter: function(req, file, callback) {
    if (file.mimetype !== "application/pdf") {
      callback(null, false);
    } else {
      callback(null, true);
    }
  }
});
