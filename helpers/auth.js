"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config")).auth;

var basicAuth = require("basic-auth");

module.exports = function() {
  return function(req, res, next) {
    if (!CONFIG.enabled) {
      return next();
    }

    var user = basicAuth(req);
    if (!user || user.name !== CONFIG.user || user.pass !== CONFIG.password) {
      res.set("WWW-Authenticate", "Basic realm=Authorization Required");
      return res.sendStatus(401);
    }
    next();
  };
};
