"use strict";

require("dotenv").config();

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

var models = require(PATH.join(__dirname, "..", "models"));

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(PATH.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "venues/create", " - Propuestas de sede");
});

router.post("/create", async (req, res) => {
  let values = {
    name: req.body.name,
    email: req.body.email,
    team: req.body.team,
    format: req.body.format,
    diversity: req.body.diversity,
    dates: req.body.dates,
    economic: req.body.economic,
    facilities: req.body.facilities,
    space: req.body.space,
    comments: req.body.comments,
    state: "R"
  };

  models.Venue.create(values).then(function(venue) {
    let file = slugify.getFileName(values.name) + ".md";
    let branch = "eslibre" + CONFIG.year + "-venues-" + venue.dataValues.id;

    gitlab.apiActions(CONFIG.mode.debug, CONFIG.language.spanish, CONFIG.type.venue, branch, file, values, res);
  });
});

module.exports = router;
