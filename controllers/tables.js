"use strict";

require("dotenv").config();

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

var models = require(PATH.join(__dirname, "..", "models"));

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(PATH.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "tables/create", " - Mesas para comunidades");
});

router.post("/create", async (req, res) => {
  let values = {
    name: req.body.community_name,
    web: req.body.web,
    community_description: req.body.community_description,
    mastodon: req.body.mastodon,
    twitter: req.body.twitter,
    gitlab: req.body.gitlab,
    contact: req.body.contact,
    email: req.body.email,
    comments: req.body.comments,
    call: req.body.call,
    accept_coc: req.body.accept_coc == "on" ? true : false,
    state: "R"
  };

  models.Table.create(values).then(function(table) {
    let file = slugify.getFileName(values.name) + ".md";
    let branch = CONFIG.event + CONFIG.year + "-tables-" + table.dataValues.id;

    if (values.call == "") {
      gitlab.apiActions(CONFIG.mode.debug, CONFIG.language.spanish, CONFIG.type.table, branch, file, values, res);
    } else {
      res.redirect(CONFIG.external_url_ok_es);
    }
  });
});

module.exports = router;
