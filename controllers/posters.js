"use strict";

require("dotenv").config();

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

var models = require(PATH.join(__dirname, "..", "models"));

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(PATH.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "posters/create", " - Pósters ilustrativos");
});

router.post("/create", async (req, res) => {
  let values = {
    title: req.body.title.replace(/:/g, ''),
    abstract: req.body.abstract,
    url: req.body.url,
    project: req.body.project,
    name: req.body.name,
    email: req.body.email,
    bio: req.body.bio,
    web: req.body.web,
    mastodon: req.body.mastodon,
    twitter: req.body.twitter,
    gitlab: req.body.gitlab,
    comments: req.body.comments,
    call: req.body.call,
    accept_coc: req.body.accept_coc == "on" ? true : false,
    state: "R"
  };

  models.Poster.create(values).then(function(poster) {
    let file = slugify.getFileName(values.title) + ".md";
    let branch = "jasyp" + CONFIG.year + "-posters-" + poster.dataValues.id;

    if (values.call == "") {
      gitlab.apiActions(CONFIG.mode.debug, CONFIG.language.spanish, CONFIG.type.poster, branch, file, values, res);
    } else {
      res.redirect(CONFIG.external_url_ok_es);
    }
  });
});

module.exports = router;
