"use strict";

require("dotenv").config();

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

var models = require(PATH.join(__dirname, "..", "models"));

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(PATH.join(__dirname, "..", "helpers", "gitlab"));
var mailer = require(PATH.join(__dirname, "..", "helpers", "mailer"));

function sendVenues(req, res, next, language) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            name: req.body.name,
            email: req.body.email,
            team: req.body.team,
            format: req.body.format,
            diversity: req.body.diversity,
            dates: req.body.dates,
            economic: req.body.economic,
            facilities: req.body.facilities,
            space: req.body.space,
            comments: req.body.comments,
            call: req.body.call,
            state: "R"
        };

        models.Venue.create(values).then(function(venue) {
            let file = slugify.getFileName(values.name) + ".md";
            let branch = "eslibre" + CONFIG.year + "-venues-" + venue.dataValues.id;

            if (values.call == "") {
                gitlab.apiActions(CONFIG.mode.production, CONFIG.language.spanish, CONFIG.type.venue, branch, file, values, res);
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

function sendProposals(req, res, next, language) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            title: req.body.title.replace(/:/g, ''),
            abstract: req.body.abstract,
            format: req.body.format,
            language: req.body.language,
            description: req.body.description,
            project: req.body.project,
            target: req.body.target,
            name: req.body.name,
            email: req.body.email,
            bio: req.body.bio,
            grants: req.body.grants,
            remote_req: req.body.remote_req,
            remote: req.body.remote,
            web: req.body.web,
            mastodon: req.body.mastodon,
            twitter: req.body.twitter,
            gitlab: req.body.gitlab,
            comments: req.body.comments,
            call: req.body.call,
            accept_coc: req.body.accept_coc == "on" ? true : false,
            state: "R"
        };

        models.Proposal.create(values).then(function(proposal) {
            let file = slugify.getFileName(values.title) + ".md";
            let branch = CONFIG.event + CONFIG.year + "-proposals-" + proposal.dataValues.id;
            let type;

            switch (values.format) {
                case 'W':
                    type = CONFIG.type.workshop;
                    break;
                case 'O':
                    type = CONFIG.type.misc;
                    break;
                default:
                    type = CONFIG.type.talk;
            }

            if (values.call == "") {
                gitlab.apiActions(CONFIG.mode.production, language, type, branch, file, values, res);
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

function sendDevrooms(req, res, next, language) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            title: req.body.title.replace(/:/g, ''),
            description: req.body.description,
            name: req.body.community_name,
            community_description: req.body.community_description,
            web: req.body.web,
            format: req.body.format,
            target: req.body.target,
            mastodon: req.body.mastodon,
            twitter: req.body.twitter,
            gitlab: req.body.gitlab,
            contact: req.body.contact,
            email: req.body.email,
            comments: req.body.comments,
            call: req.body.call,
            accept_coc: req.body.accept_coc == "on" ? true : false,
            state: "R"
        };

        models.Devroom.create(values).then(function(devroom) {
            let file = slugify.getFileName(values.title) + ".md";
            let branch = CONFIG.event + CONFIG.year + "-devrooms-" + devroom.dataValues.id;

            if (values.call == "") {
                gitlab.apiActions(CONFIG.mode.production, language, CONFIG.type.devroom, branch, file, values, res);
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

function sendTables(req, res, next, language) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            name: req.body.community_name,
            web: req.body.web,
            community_description: req.body.community_description,
            mastodon: req.body.mastodon,
            twitter: req.body.twitter,
            gitlab: req.body.gitlab,
            contact: req.body.contact,
            email: req.body.email,
            comments: req.body.comments,
            call: req.body.call,
            accept_coc: req.body.accept_coc == "on" ? true : false,
            state: "R"
        };

        models.Table.create(values).then(function(table) {
            let file = slugify.getFileName(values.name) + ".md";
            let branch = CONFIG.event + CONFIG.year + "-tables-" + table.dataValues.id;

            if (values.call == "") {
                gitlab.apiActions(CONFIG.mode.production, language, CONFIG.type.table, branch, file, values, res);
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

function sendMisc(req, res, next, language) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            title: req.body.title.replace(/:/g, ''),
            abstract: req.body.abstract,
            format: req.body.format,
            description: req.body.description,
            project: req.body.project,
            target: req.body.target,
            name: req.body.name,
            email: req.body.email,
            bio: req.body.bio,
            web: req.body.web,
            mastodon: req.body.mastodon,
            twitter: req.body.twitter,
            gitlab: req.body.gitlab,
            comments: req.body.comments,
            call: req.body.call,
            accept_coc: req.body.accept_coc == "on" ? true : false,
            state: "R"
        };

        models.Misc.create(values).then(function(misc) {
            let file = slugify.getFileName(values.title) + ".md";
            let branch = CONFIG.event + CONFIG.year + "-miscs-" + misc.dataValues.id;

            if (values.call == "") {
                gitlab.apiActions(CONFIG.mode.production, language, CONFIG.type.misc, branch, file, values, res);
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

function sendGrants(req, res, next) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            name: req.body.name,
            email: req.body.email,
            bio: req.body.bio,
            call: req.body.call,
            state: "R"
        };

        models.Grants.create(values).then(function(grants) {
            if (values.call == "") {
                mailer.getMailerGrants(values).sendMail({}, function(error, info) {
                    if (error) {
                        handlers.errorHandlerCreate(CONFIG.mode.production, CONFIG.language.spanish, res, error);
                    } else {
                        res.redirect("https://eslib.re/2024/bolsa-recibida");
                    }
                });
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

function sendCaring(req, res, next) {
    let validIP = process.env.VALID_IP.split(",");
    let currentIp = req.connection.remoteAddress;
    currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

    if (validIP.includes(currentIp)) {
        let values = {
            name: req.body.name,
            email: req.body.email,
            age: req.body.age,
            hours: req.body.hours,
            call: req.body.call,
            state: "R"
        };

        models.Caring.create(values).then(function(caring) {
            if (values.call == "") {
                mailer.getMailerCaring(values).sendMail({}, function(error, info) {
                    if (error) {
                        handlers.errorHandlerCreate(CONFIG.mode.production, CONFIG.language.spanish, res, error);
                    } else {
                        res.redirect("https://eslib.re/2024/cuidados-recibida");
                    }
                });
            } else {
                res.redirect(CONFIG.external_url_ok_es);
            }
        });
    } else {
        res.send({
            somos: "ruido"
        });
    }
}

router.get("/", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/proposals-es", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/proposals-en", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/proposals-gl", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/devrooms-es", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/devrooms-en", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/devrooms-gl", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/tables-es", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/tables-en", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/misc-es", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/misc-en", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/misc-gl", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/grants-attendance", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/care-service", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

router.get("/venues-es", function(req, res, next) {
    res.send({
        somos: "ruido"
    });
});

/*
router.get("/arts-es", function(req, res, next) {
  res.send({
    somos: "ruido"
  });
});

router.get("/posters-es", function(req, res, next) {
  res.send({
    somos: "ruido"
  });
});
*/

router.post("/venues-es", function(req, res, next) {
    sendVenues(req, res, next, CONFIG.language.spanish);
});

router.post("/proposals-es", function(req, res, next) {
    sendProposals(req, res, next, CONFIG.language.spanish);
});

router.post("/proposals-en", function(req, res, next) {
    sendProposals(req, res, next, CONFIG.language.english);
});

router.post("/proposals-gl", function(req, res, next) {
    sendProposals(req, res, next, CONFIG.language.galician);
});

router.post("/devrooms-es", function(req, res, next) {
    sendDevrooms(req, res, next, CONFIG.language.spanish);
});

router.post("/devrooms-en", function(req, res, next) {
    sendDevrooms(req, res, next, CONFIG.language.english);
});

router.post("/devrooms-gl", function(req, res, next) {
    sendDevrooms(req, res, next, CONFIG.language.galician);
});

router.post("/tables-es", function(req, res, next) {
    sendTables(req, res, next, CONFIG.language.spanish);
});

router.post("/tables-en", function(req, res, next) {
    sendTables(req, res, next, CONFIG.language.english);
});

router.post("/misc-es", function(req, res, next) {
    sendMisc(req, res, next, CONFIG.language.spanish);
});

router.post("/misc-en", function(req, res, next) {
    sendMisc(req, res, next, CONFIG.language.english);
});

router.post("/misc-gl", function(req, res, next) {
    sendMisc(req, res, next, CONFIG.language.galician);
});

router.post("/grants-attendance", function(req, res, next) {
    sendGrants(req, res, next);
});

router.post("/care-service", function(req, res, next) {
    sendCaring(req, res, next);
});

/*
router.post("/posters-es", async (req, res) => {
  let validIP = process.env.VALID_IP.split(",");
  let currentIp = req.connection.remoteAddress;
  currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

  if (validIP.includes(currentIp)) {
    let values = {
      title: req.body.title.replace(/:/g, ''),
      abstract: req.body.abstract,
      url: req.body.url,
      project: req.body.project,
      name: req.body.name,
      email: req.body.email,
      bio: req.body.bio,
      web: req.body.web,
      mastodon: req.body.mastodon,
      twitter: req.body.twitter,
      gitlab: req.body.gitlab,
      comments: req.body.comments,
      call: req.body.call,
      accept_coc: req.body.accept_coc == "on" ? true : false,
      state: "R"
    };

    models.Poster.create(values).then(function(poster) {
      let file = slugify.getFileName(values.title) + ".md";
      let branch = "jasyp" + CONFIG.year + "-posters-" + poster.dataValues.id;

      if (values.call == "") {
        gitlab.apiActions(CONFIG.mode.production, CONFIG.language.spanish, CONFIG.type.poster, branch, file, values, res);
      } else {
        res.redirect(CONFIG.external_url_ok_es);
      }
    });
  } else {
    res.send({
      somos: "ruido"
    });
  }
});

router.post("/arts-es", async (req, res) => {
  let validIP = process.env.VALID_IP.split(",");
  let currentIp = req.connection.remoteAddress;
  currentIp = currentIp.substring(currentIp.lastIndexOf(":") + 1, currentIp.length);

  if (validIP.includes(currentIp)) {
    let values = {
      title: req.body.title.replace(/:/g, ''),
      abstract: req.body.abstract,
      url: req.body.url,
      name: req.body.name,
      email: req.body.email,
      bio: req.body.bio,
      web: req.body.web,
      mastodon: req.body.mastodon,
      twitter: req.body.twitter,
      gitlab: req.body.gitlab,
      comments: req.body.comments,
      call: req.body.call,
      accept_coc: req.body.accept_coc == "on" ? true : false,
      state: "R"
    };

    models.Art.create(values).then(function(art) {
      let file = slugify.getFileName(values.title) + ".md";
      let branch = "jasyp" + CONFIG.year + "-arts-" + art.dataValues.id;

      if (values.call == "") {
        gitlab.apiActions(CONFIG.mode.production, CONFIG.language.spanish, CONFIG.type.art, branch, file, values, res);
      } else {
        res.redirect(CONFIG.external_url_ok_es);
      }
    });
  } else {
    res.send({
      somos: "ruido"
    });
  }
});
*/

module.exports = router;
