"use strict";

require("dotenv").config();

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

var models = require(PATH.join(__dirname, "..", "models"));

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(PATH.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "devrooms/create", " - Envío de charlas y talleres");
});

router.post("/create", async (req, res) => {
  let values = {
    title: req.body.title.replace(/:/g, ''),
    description: req.body.description,
    name: req.body.community_name,
    community_description: req.body.community_description,
    web: req.body.web,
    format: req.body.format,
    target: req.body.target,
    mastodon: req.body.mastodon,
    twitter: req.body.twitter,
    gitlab: req.body.gitlab,
    contact: req.body.contact,
    email: req.body.email,
    comments: req.body.comments,
    call: req.body.call,
    accept_coc: req.body.accept_coc == "on" ? true : false,
    state: "R"
  };

  models.Devroom.create(values).then(function(devroom) {
    let file = slugify.getFileName(values.title) + ".md";
    let branch = CONFIG.event + CONFIG.year + "-devrooms-" + devroom.dataValues.id;

    if (values.call == "") {
      gitlab.apiActions(CONFIG.mode.debug, CONFIG.language.spanish, CONFIG.type.devroom, branch, file, values, res);
    } else {
      res.redirect(CONFIG.external_url_ok_es);
    }
  });
});

module.exports = router;
