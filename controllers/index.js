"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

router.get("/", function(req, res, next) {
  res.render("pages/index", {
    title: CONFIG.event_name
  });
});

module.exports = router;