"use strict";

require("dotenv").config();

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var express = require("express");
var router = express.Router();

var models = require(PATH.join(__dirname, "..", "models"));

var handlers = require(PATH.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(PATH.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(PATH.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "proposals/create", " - Envío de charlas y talleres");
});

router.post("/create", async (req, res) => {
  let values = {
    title: req.body.title.replace(/:/g, ''),
    abstract: req.body.abstract,
    format: req.body.format,
    description: req.body.description,
    project: req.body.project,
    target: req.body.target,
    name: req.body.name,
    email: req.body.email,
    bio: req.body.bio,
    grants: req.body.grants,
    remote_req: req.body.remote_req,
    remote: req.body.remote,
    web: req.body.web,
    mastodon: req.body.mastodon,
    twitter: req.body.twitter,
    gitlab: req.body.gitlab,
    comments: req.body.comments,
    call: req.body.call,
    accept_coc: req.body.accept_coc == "on" ? true : false,
    state: "R"
  };

  models.Proposal.create(values).then(function(proposal) {
    let file = slugify.getFileName(values.title) + ".md";
    let branch = CONFIG.event + CONFIG.year + "-proposals-" + proposal.dataValues.id;
    let type;

    switch (values.format) {
      case 'W':
        type = CONFIG.type.workshop;
        break;
      case 'O':
        type = CONFIG.type.misc;
        break;
      default:
        type = CONFIG.type.talk;
    }

    if (values.call == "") {
      gitlab.apiActions(CONFIG.mode.debug, CONFIG.language.spanish, type, branch, file, values, res);
    } else {
      res.redirect(CONFIG.external_url_ok_es);
    }
  });
});

module.exports = router;
