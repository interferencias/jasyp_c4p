"use strict";

module.exports = (sequelize, DataTypes) => {
  var Table = sequelize.define("Table", {
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    web: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    community_description: {
      type: DataTypes.TEXT("medium"),
      allowNull: false
    },
    mastodon: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    twitter: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    gitlab: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    contact: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    comments: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    call: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    accept_coc: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Table;
};
