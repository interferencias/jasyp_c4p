"use strict";

module.exports = (sequelize, DataTypes) => {
  var Proposal = sequelize.define("Proposal", {
    title: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    abstract: {
      type: DataTypes.TEXT("medium"),
      allowNull: false
    },
    format: {
      // S = Short talk, L = Large talk, W = Workshop, O = Other
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    language: {
      // S = Spanish, C = catalan, G = galician, V = valencian, B = basque, A = aranese, O = other
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT("medium"),
      allowNull: false
    },
    project: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    target: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    bio: {
      type: DataTypes.TEXT("medium"),
      allowNull: false
    },
    grants: {
      // N = No, S = Minority groups, G = General help
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    remote_req: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    remote: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    web: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    mastodon: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    twitter: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    gitlab: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    comments: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    call: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    accept_coc: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Proposal;
};
