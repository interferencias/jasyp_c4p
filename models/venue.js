"use strict";

module.exports = (sequelize, DataTypes) => {
  var Venue = sequelize.define("Venue", {
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    team: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    format: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    diversity: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    dates: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    economic: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    facilities: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    space: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    comments: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Venue;
};
