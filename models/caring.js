"use strict";

module.exports = (sequelize, DataTypes) => {
  var Caring = sequelize.define("Caring", {
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    age: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    hours: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Caring;
};
