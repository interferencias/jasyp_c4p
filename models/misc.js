"use strict";

module.exports = (sequelize, DataTypes) => {
  var Misc = sequelize.define("Misc", {
    title: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    abstract: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    format: {
      // A = Artículo, P = Pósters, T = Tablón de proyectos, O = Otros
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT("medium"),
      allowNull: false
    },
    project: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    target: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    bio: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    web: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    mastodon: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    twitter: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    gitlab: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    comments: {
      type: DataTypes.TEXT("medium"),
      allowNull: true
    },
    call: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    accept_coc: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Misc;
};
