"use strict";

const PATH = require("path");
const CONFIG = require(PATH.join(__dirname, "..", "config", "config"));

var fs = require("fs");
var Sequelize = require("sequelize");

var basename = PATH.basename(__filename);
var db = {};
var env = process.env.NODE_ENV || "development";
var sequelize;

if (env.localeCompare("development") === 0) {
  sequelize = new Sequelize("debug", null, null, CONFIG.development);
} else {
  sequelize = new Sequelize(CONFIG.production.database, CONFIG.production.username, CONFIG.production.password, CONFIG.production);
}

fs.readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf(".") !== 0) && (file !== basename) && (file.slice(-3) === ".js");
  })
  .forEach(file => {
    var model = require(PATH.join(__dirname, file))(sequelize, Sequelize.DataTypes)

    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
