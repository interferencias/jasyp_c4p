"use strict";

module.exports = (sequelize, DataTypes) => {
  var Grants = sequelize.define("Grants", {
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    bio: {
      type: DataTypes.TEXT("medium"),
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Grants;
};