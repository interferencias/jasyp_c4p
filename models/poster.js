"use strict";

module.exports = (sequelize, DataTypes) => {
  var Poster = sequelize.define("Poster", {
    title: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    abstract: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    project: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    bio: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    web: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    mastodon: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    twitter: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    gitlab: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    comments: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    call: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    accept_coc: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Poster;
};
