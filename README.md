# Interferencias C4P

Aplicación para gestionar call for papers

### Instalación

    npm install

### Modo debug

Funcionamiento a través de **nodemon**.

    npm run debug

### Modo producción

Funcionamiento a través de **PM2**.

    npm start
