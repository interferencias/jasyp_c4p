"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var favicon = require("serve-favicon");
var logger = require("morgan");
var path = require("path");

var auth = require(path.join(__dirname, "helpers", "auth"));
var handlers = require(path.join(__dirname, "helpers", "handlers"));

// generals controllers
var blank = require(path.join(__dirname, "controllers", "blank"));
var index = require(path.join(__dirname, "controllers", "index"));
var form = require(path.join(__dirname, "controllers", "form"));
// specific controllers
var arts = require(path.join(__dirname, "controllers", "arts"));
var devrooms = require(path.join(__dirname, "controllers", "devrooms"));
var tables = require(path.join(__dirname, "controllers", "tables"));
var miscs = require(path.join(__dirname, "controllers", "miscs"));
var posters = require(path.join(__dirname, "controllers", "posters"));
var proposals = require(path.join(__dirname, "controllers", "proposals"));
var venues = require(path.join(__dirname, "controllers", "venues"));

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, "assets", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "assets")));

// general paths
app.use("/", blank);
app.use("/form", form);
// protected paths
app.use(auth());
app.use("/admin/", index);
app.use("/admin/arts", arts);
app.use("/admin/devrooms", devrooms);
app.use("/admin/tables", tables);
app.use("/admin/miscs", miscs);
app.use("/admin/posters", posters);
app.use("/admin/proposals", proposals);
app.use("/admin/venues", venues);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  handlers.errorHandlerCreate(0, 1, res, err);
});

module.exports = app;
