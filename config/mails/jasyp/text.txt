Hola:

¡Muchas gracias por tu aportación a la sala Derechos Digitales, Privacidad en Internet y Seguridad Informática que estamos
organizando dentro de "eslibre 2020"(https: //eslib.re/2020/)! Como el propio congreso es dentro de un mes (18 y 19 de septiembre),
y sabemos que es poco tiempo, en un par de días te confirmaremos si nos parece que la charla encaje en el planteamiento de la sala
y el congreso.

Hemos recibido la siguiente propuesta:

  · Nombre: _name
  · Email: _email
  · Título: _title
  · Duración: _length
  · Resumen: _abstract

Si alguno de estos datos es erróneo, quieres cambiar algo o tienes cualquier duda, puedes escribirnos a info @interferencias.tech.

Nos vemos(virtualmente😉) dentro de poco.Saludos,
    Interferencias
