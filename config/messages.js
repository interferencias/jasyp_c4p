"use strict";

let abstract = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="abstract">Resumen (*):</label></strong> <div id="abstract-counter-tag" class="tag is-danger"><span id="abstract-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;2000&nbsp;</div><textarea name="abstract" cols="40" rows="5" maxlength="2000" style="resize:none; overflow-y: scroll;" aria-describedby="abstract-help-block" onkeyup="counterDisplay(this, \'abstract\', true)" required="required" class="form-control"></textarea><span id="abstract-help-block" class="form-text text-muted list-forms-text">Pequeña introducción y motivación de la propuesta.</span> </div>';

let abstract_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="abstract">Resumen (*):</label> </strong> <div id="abstract-counter-tag" class="tag is-danger"> <span id="abstract-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000&nbsp;</div> <textarea name="abstract" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="abstract-help-block" onkeyup="counterDisplay(this, \'abstract\', true)" required="required" class="form-control"> </textarea> <span id="abstract-help-block" class="form-text text-muted list-forms-text">Pequeña introducción y motivación de la propuesta.</span> </div>';

let accept_coc = '<div class="form-group"> <div> <div class="custom-control custom-checkbox"><input name="accept_coc" type="checkbox" required="required"><label class="label-forms-text" for="accept_coc">&nbsp;Acepto seguir el <a href="/conducta" target="_blank">código de conducta</a> durante mi participación en las jornadas (*)</label></div> </div> </div>';

let accept_coc_proposals = '<div class="form-group"> <div> <div class="custom-control custom-checkbox"> <input name="accept_coc" type="checkbox" required="required"> <label class="label-forms-text" for="accept_coc">&nbsp;Acepto seguir el <a href="/conducta" target="_blank">código de conducta</a> durante mi participación en el congreso (*)</label> </div> </div> </div>';

let accept_coc_devrooms = '<div class="form-group"> <div> <div class="custom-control custom-checkbox"> <input name="accept_coc" type="checkbox" required="required"> <label class="label-forms-text" for="accept_coc">&nbsp;Aceptamos seguir el <a href="/conducta" target="_blank">código de conducta</a> durante nuestra participación en el congreso(*)</label> </div> </div> </div>';

let bio = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="bio">Bio:</label></strong> <div id="bio-counter-tag" class="tag is-success"><span id="bio-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;2000&nbsp;</div><textarea name="bio" cols="40" rows="5" maxlength="2000" style="resize:none; overflow-y: scroll;" aria-describedby="bio-help-block" onkeyup="counterDisplay(this, \'bio\', false)" class="form-control"></textarea><span id="bio-help-block" class="form-text text-muted list-forms-text">Por si quieres añadir alguna información sobre ti: intereses personales, proyectos relacionados, experiencias varias...</span> </div>';

let bio_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="bio">Bio (*):</label> </strong> <div id="bio-counter-tag" class="tag is-danger"> <span id="bio-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;2000&nbsp;</div> <textarea name="bio" cols="40" rows="5" maxlength="2000" style="resize:none; overflow-y: scroll;" aria-describedby="bio-help-block" onkeyup="counterDisplay(this, \'bio\', true)" required="required" class="form-control"> </textarea> <span id="bio-help-block" class="form-text text-muted list-forms-text">Necesitaríamos algo de información general sobre la persona o personas que van a llevar a cabo la propuesta: intereses personales, experiencia en la materia, si has realizado actividades similares...</span> </div>';

let bio_req = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="bio">Bio (*):</label></strong> <div id="bio-counter-tag" class="tag is-danger"><span id="bio-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000&nbsp;</div><textarea name="bio" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="bio-help-block" onkeyup="counterDisplay(this, \'bio\', true)" required="required" class="form-control"></textarea><span id="bio-help-block" class="form-text text-muted list-forms-text">Necesitaríamos algo de información general sobre la persona o personas que van a llevar a cabo la propuesta: intereses personales, experiencia en la materia, si has realizado actividades similares...</span> </div>';

let call = '<div class="call-outer"> <div class="form-group call-inner" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="call">Llamada:</label></strong> <div id="call-counter-tag" class="tag is-success"><span id="call-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="call" type="call" maxlength="150" onkeyup="counterDisplay(this, \'call\', true)"class="form-control"> </div> </div>';

let comments = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="comments">Comentarios:</label></strong> <div id="comments-counter-tag" class="tag is-success"><span id="comments-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="comments" cols="40" rows="5" maxlength="500" style="resize:none; overflow-y: scroll;" aria-describedby="comments-help-block" onkeyup="counterDisplay(this, \'comments\', false)" class="form-control"></textarea><span id="comments-help-block" class="form-text text-muted list-forms-text">Cualquier otro comentario relevante para la organización.</span> </div>';

let comments_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="comments">Comentarios:</label> </strong> <div id="comments-counter-tag" class="tag is-success"> <span id="comments-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000&nbsp;</div> <textarea name="comments" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="comments-help-block" onkeyup="counterDisplay(this, \'comments\', false)" class="form-control"> </textarea> <span id="comments-help-block" class="form-text text-muted list-forms-text">Cualquier otro comentario relevante para la organización.</span> </div>';

let comments_devrooms = '<div class="form-group"> <strong> <label for="comments">Comentarios:</label> </strong> <div id="comments-counter-tag" class="tag is-success"> <span id="comments-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000</div> <textarea name="comments" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="comments-help-block" onkeyup="counterDisplay(this, \'comments\', false)" class="form-control"> </textarea> <span id="comments-help-block" class="form-text text-muted">Cualquier otro comentario relevante para la organización.</span> </div>';

let community_description_devrooms = '<div class="form-group"> <strong> <label for="community_description">Info sobre vuestra comunidad (*):</label> </strong> <div id="community_description-counter-tag" class="tag is-danger"> <span id="community_description-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;5000</div> <textarea name="community_description" cols="40" rows="5" maxlength="5000" style="resize:none; overflow-y: scroll;" aria-describedby="community_description-help-block" onkeyup="counterDisplay(this, \'community_description\', true)" required="required" class="form-control"> </textarea> <span id="community_description-help-block" class="form-text text-muted">Información sobre el grupo que propone la organización de la sala, así como otras actividades organizadas. Sería conveniente indicar también las personas que van a estar encargadas de la misma.</span> </div>';

let community_name_devrooms = '<div class="form-group"> <strong> <label for="community_name">Comunidad que la propone (*):</label> </strong> <div id="community_name-counter-tag" class="tag is-danger"> <span id="community_name-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150</div> <input id="community_name" name="community_name" type="text" maxlength="150" onkeyup="counterDisplay(this, \'community_name\', true)" required="required" class="form-control"> </div>';

let contact_devrooms = '<div class="form-group"> <strong> <label for="contact">Persona de contacto (*):</label> </strong> <div id="contact-counter-tag" class="tag is-danger"> <span id="contact-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150</div> <input name="contact" type="text" maxlength="150" onkeyup="counterDisplay(this, \'contact\', true)" required="required" class="form-control"> <span id="contact-help-block" class="form-text text-muted list-forms-text">Persona que hará de contacto directo con la organización.</span> </div>';

let description = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="description">Descripción (*):</label></strong> <div id="description-counter-tag" class="tag is-danger"><span id="description-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;2000&nbsp;</div><textarea name="description" cols="40" rows="5" maxlength="2000" style="resize:none; overflow-y: scroll;" aria-describedby="description-help-block" onkeyup="counterDisplay(this, \'description\', true)" required="required" class="form-control"></textarea><span id="description-help-block" class="form-text text-muted list-forms-text">Descripción algo más extensa sobre la temática y el contenido de la propuesta (si es un taller indica también su duración: puede ser desde 30 hasta un máximo de 60 minutos).</span> </div>';

let description_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="description">Descripción (*):</label> </strong> <div id="description-counter-tag" class="tag is-danger"> <span id="description-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;5000&nbsp;</div> <textarea name="description" cols="40" rows="5" maxlength="5000" style="resize:none; overflow-y: scroll;" aria-describedby="description-help-block" onkeyup="counterDisplay(this, \'description\', true)" required="required" class="form-control"> </textarea> <span id="description-help-block" class="form-text text-muted list-forms-text">Descripción algo más extensa sobre la temática y el contenido de la propuesta.</span> </div>';

let description_devrooms = '<div class="form-group"> <strong> <label for="description">Descripción de la sala (*):</label> </strong> <div id="description-counter-tag" class="tag is-danger"> <span id="description-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;5000</div> <textarea name="description" cols="40" rows="5" maxlength="5000" style="resize:none; overflow-y: scroll;" aria-describedby="description-help-block" onkeyup="counterDisplay(this, \'description\', true)" required="required" class="form-control"> </textarea> <span id="description-help-block" class="form-text text-muted">Explicación de la temática de la sala y la motivación para organizarla. Tened en cuenta que la idea de la sala es realizar una actividad semiautónoma dentro de esLibre, que tendrá normalmente petición de propuestas propia, equipo de organización propio, horario propio (coordinado con esLibre)...</span> </div>';

let disclaimer_venue = '<div class="form-group">' +
  '<span class="form-text text-muted"' +
  '<ul style="list-style-position: inside; padding-left: 0">' +
  '<li>Todos los campos son obligatorios, pero la información se podrá modificar una vez enviada</li>' +
  '<li>Una vez enviada la propuesta, mientras que es revisada se podrá encontrar en <a href="https://gitlab.com/eslibre/coord/-/merge_requests?scope=all&state=opened&label_name[]=propuestas-2022" target="_blank">esta página</a></li>' +
  '<li>Recuerda que para cualquier duda puedes escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a></li>' +
  '</ul>' +
  '</span>' +
  '</div>';

let email = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="email">Email (*):</label></strong> <div id="email-counter-tag" class="tag is-danger"><span id="email-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="email" type="email" maxlength="150" onkeyup="counterDisplay(this, \'email\', true)" required="required" class="form-control"><span id="project-help-block" class="form-text text-muted list-forms-text">Lo necesitamos para comunicarnos contigo, pero no será publicado en ningún momento.</span> </div>';

let email_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="email">Email (*):</label> </strong> <div id="email-counter-tag" class="tag is-danger"> <span id="email-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="email" type="email" maxlength="150" onkeyup="counterDisplay(this, \'email\', true)" required="required" class="form-control"> <span id="project-help-block" class="form-text text-muted list-forms-text">Lo necesitamos para comunicarnos contigo, pero no será publicado en ningún momento.</span> </div>';

let email_devrooms = '<div class="form-group"> <strong> <label for="email">Email de contacto (*):</label> </strong> <div id="email-counter-tag" class="tag is-danger"> <span id="email-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150</div> <input name="email" type="email" maxlength="150" onkeyup="counterDisplay(this, \'email\', true)" required="required" class="form-control"> <span id="contact-help-block" class="form-text text-muted list-forms-text">Correo para comunicarnos con vosotros (no será público).</span> </div>';

let format = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text">Formato (*):</label></strong> <div> <div class="custom-control label-check"><input name="format" type="radio" required="required" value="S"><label for="format">&nbsp;Charla corta (20 minutos)</label></div> <div class="custom-control label-check"><input name="format" type="radio" required="required" value="L"><label for="format">&nbsp;Charla larga (40 minutos)</label> </div> <div class="custom-control label-check"><input name="format" type="radio" required="required" value="W"><label for="format">&nbsp;Taller (30-60 minutos)</label> </div> <div class="custom-control label-check"><input name="format" type="radio" required="required" value="O"><label for="format">&nbsp;Otra (por favor, detalla el formato abajo)</label> </div> </div> </div>';

let format_proposals = '<div class="form-group"> <strong> <label class="label-forms-text">Formato (*):</label> </strong> <div> <div class="custom-control label-check"> <input name="format" type="radio" required="required" value="S"> <label for="format">&nbsp;Charla corta (entre 10 y 15 minutos)</label> </div> <div class="custom-control label-check"> <input name="format" type="radio" required="required" value="L"> <label for="format">&nbsp;Charla larga (alrededor de 30 minutos)</label> </div> <div class="custom-control label-check"> <input name="format" type="radio" required="required" value="W"> <label for="format">&nbsp;Taller (indica duración en la descripción: máximo 2 horas)</label> </div> </div> </div>';

let format_devrooms = '<div class="form-group"> <strong> <label for="format">Formato (*):</label> </strong> <div id="format-counter-tag" class="tag is-danger"> <span id="format-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;5000</div> <textarea name="format" cols="40" rows="5" maxlength="5000" style="resize:none; overflow-y: scroll;" aria-describedby="format-help-block" onkeyup="counterDisplay(this, \'format\', true)" required="required" class="form-control"> </textarea> <span id="format-help-block" class="form-text text-muted">Formato de la sala y cómo se va a hacer la solicitud de propuestas. El formato se deja totalmente abierto a decisión de la organización de la sala, pudiendo incluir: talleres, charlas cortas/largas, mesas redondas, hackathones...<br> <strong>IMPORTANTE</strong>: incluir también la duración que tendría (desde dos horas hasta un día entero) y el día que preferís realizarla (viernes o sábado).</span> </div>';

let format_miscs = '<div class="form-group"><strong><label class="label-forms-text">Tipo de actividad (*):</label></strong><div><div class="custom-control label-check"><input name="format" type="radio" required="required" value="A"><label for="format">&nbsp;Artículo</label></div><div class="custom-control label-check"><input name="format" type="radio" required="required" value="P"><label for="format">&nbsp;Pósters</label> </div><div class="custom-control label-check"><input name="format" type="radio" required="required" value="T"><label for="format">&nbsp;Tablón de proyectos</label> </div><div class="custom-control label-check"><input name="format" type="radio" required="required" value="O"><label for="format">&nbsp;Otros formatos (explícalo en la descripción)</label> </div></div></div>';

let format_venue = '<div class="form-group">' +
  '<strong><label for="format">Formato del congreso:</label></strong>' +
  '<div id="format-counter-tag" class="tag is-danger">' +
  '<span id="format-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;5000&nbsp;' +
  '</div>' +
  '<textarea name="format" cols="40" rows="5" maxlength="500" style="resize:none; overflow-y: scroll;" aria-describedby="format-help-block" onkeyup="counterDisplay(this, \'format\', true)" required="required" class="form-control"></textarea>' +
  '<span id="format-help-block" class="form-text text-muted">' +
  'Ideas para adaptarlas al formato del congreso, posibilidad para la realización de un formato mixto...' +
  '</span>' +
  '</div>';

let gitlab = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="gitlab">GitLab (o cualquier sitio de código colaborativo) o portfolio:</label></strong> <div id="gitlab-counter-tag" class="tag is-success"><span id="gitlab-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="gitlab" type="text" maxlength="255" aria-describedby="gitlab-help-block" onkeyup="counterDisplay(this, \'gitlab\', false)" class="form-control"><span id="gitlab-help-block" class="form-text text-muted list-forms-text">Por si quieres compartir tu portfolio o cualquier otra de tus obras.</span> </div>';

let gitlab_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="gitlab">GitLab (o cualquier sitio de código colaborativo) o portfolio:</label> </strong> <div id="gitlab-counter-tag" class="tag is-success"> <span id="gitlab-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="gitlab" type="text" maxlength="255" aria-describedby="gitlab-help-block" onkeyup="counterDisplay(this, \'gitlab\', false)" class="form-control"> <span id="gitlab-help-block" class="form-text text-muted list-forms-text">Por si quieres compartir tus proyectos.</span> </div>';

let gitlab_devrooms = '<div class="form-group"> <strong> <label class="label-forms-text" for="gitlab">GitLab (o cualquier sitio de código colaborativo) o portfolio:</label> </strong> <div id="gitlab-counter-tag" class="tag is-success"> <span id="gitlab-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="gitlab" type="text" maxlength="255" aria-describedby="gitlab-help-block" onkeyup="counterDisplay(this, \'gitlab\', false)" class="form-control"> <span id="gitlab-help-block" class="form-text text-muted list-forms-text">Por si queréis compartir vuestros proyectos.</span> </div>';

let grants = '<div class="form-group"> <strong> <label class="label-forms-text">Solicitud de fondos para participar:</label> </strong> <div> <div class="custom-control label-check"> <input name="grants" type="radio" required="required" value="N" checked="checked"> <label for="grants">&nbsp;No solicito ninguna ayuda</label> </div> <div class="custom-control label-check"> <input name="grants" type="radio" required="required" value="S"> <label for="grants">&nbsp;Ayuda específica grupos de personas menos representados (totalidad de gastos)</label> </div> <div class="custom-control label-check"> <input name="grants" type="radio" required="required" value="G"> <label for="grants">&nbsp;Ayuda general para asistir al congreso (proporcional según a criterios de la organización)</label> </div> </div> <span id="grants-help-block" class="form-text text-muted list-forms-text">Solicitar fondos no implica la concesión automática de lo mismos, pero la organización se pondrá en contacto contigo para estudiar tu caso de forma personalizada. Pues encontrar más información al respecto en <a href="/2022/bolsa-viaje/" target="_blank">esta página</a>.</span> </div>';

let mastodon = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="mastodon">Mastodon (u otras redes sociales libres):</label></strong> <div id="mastodon-counter-tag" class="tag is-success"><span id="mastodon-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="mastodon" type="url" maxlength="150" aria-describedby="mastodon-help-block" onkeyup="counterDisplay(this, \'mastodon\', false)" class="form-control"><span id="mastodon-help-block" class="form-text text-muted list-forms-text">Cualquier perfil en una red social libre es válido, pero recuerda introducir la URL completa para conocer también la instancia en la que te encuentras para que podamos encontrarte.</span> </div>';

let mastodon_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="mastodon">Mastodon (u otras redes sociales libres):</label> </strong> <div id="mastodon-counter-tag" class="tag is-success"> <span id="mastodon-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="mastodon" type="url" maxlength="150" aria-describedby="mastodon-help-block" onkeyup="counterDisplay(this, \'mastodon\', false)" class="form-control"> <span id="mastodon-help-block" class="form-text text-muted list-forms-text">Cualquier perfil en una red social libre es válido, pero recuerda introducir la URL completa para conocer también la instancia en la que te encuentras para que podamos encontrarte.</span> </div>';

let mastodon_devrooms = '<div class="form-group"> <strong> <label for="mastodon">Mastodon (u otras redes sociales libres):</label> </strong> <div id="mastodon-counter-tag" class="tag is-success"> <span id="mastodon-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150</div> <input name="mastodon" type="url" maxlength="150" aria-describedby="mastodon-help-block" onkeyup="counterDisplay(this, \'mastodon\', false)" class="form-control"> <span id="mastodon-help-block" class="form-text text-muted">Cualquier perfil en una red social libre es válido, pero recordad introducir la URL completa para conocer también la instancia en la que os encontráis para que os podamos encontrar.</span> </div>';

let name = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="name">Nombre (*):</label></strong> <div id="name-counter-tag" class="tag is-danger"><span id="name-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="name" type="text" maxlength="150" onkeyup="counterDisplay(this, \'name\', true)" required="required" class="form-control"><span id="project-help-block" class="form-text text-muted list-forms-text">Puedes usar un seudónimo si lo prefieres.</span> </div>';

let name_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="name">Nombre (*):</label> </strong> <div id="name-counter-tag" class="tag is-danger"> <span id="name-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="name" type="text" maxlength="150" onkeyup="counterDisplay(this, \'name\', true)" required="required" class="form-control"> <span id="project-help-block" class="form-text text-muted list-forms-text">Puedes usar un seudónimo si lo prefieres.</span> </div>';

let name_venue = '<div class="form-group">' +
  '<label for="name">Nombre del equipo (*):</label>' +
  '<div id="name-counter-tag" class="tag is-danger"><span id="name-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div>' +
  '<input name="name" type="text" maxlength="150" onkeyup="counterDisplay(this, \'name\', true)" required="required" class="form-control">' +
  '</div>';

let portfolio = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="gitlab">Portfolio:</label></strong> <div id="gitlab-counter-tag" class="tag is-success"><span id="gitlab-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="gitlab" type="text" maxlength="255" aria-describedby="gitlab-help-block" onkeyup="counterDisplay(this, \'gitlab\', false)" class="form-control"><span id="gitlab-help-block" class="form-text text-muted list-forms-text">Por si quieres compartir tu portfolio o cualquier otra de tus obras.</span> </div>';

let project = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="project">Web del proyecto:</label></strong> <div id="project-counter-tag" class="tag is-success"><span id="project-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="project" type="url" maxlength="150" aria-describedby="project-help-block" onkeyup="counterDisplay(this, \'project\', false)" class="form-control"><span id="project-help-block" class="form-text text-muted list-forms-text">Página con información sobre el proyecto (si la hay).</span> </div>';

let project_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="project">Web del proyecto:</label> </strong> <div id="project-counter-tag" class="tag is-success"> <span id="project-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="project" type="url" maxlength="150" aria-describedby="project-help-block" onkeyup="counterDisplay(this, \'project\', false)" class="form-control"> <span id="project-help-block" class="form-text text-muted list-forms-text">Página con información sobre el proyecto (si la hay).</span> </div>';

let remote = '<div class="form-group"> <strong> <label class="label-forms-text">Solicitud de participación en remoto:</label> </strong> <div id="remote-counter-tag" class="tag is-success"> <span id="remote-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000&nbsp;</div> <div> <div class="custom-control custom-checkbox"> <input name="remote_req" type="checkbox"> <label class="label-forms-text" for="remote_req">&nbsp;Solicito participar en remoto en el congreso por las siguientes razones:</label> </div> </div> <textarea name="remote" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="remote-help-block" onkeyup="counterDisplay(this, \'remote\', false)" class="form-control"> </textarea> <span id="remote-help-block" class="form-text text-muted list-forms-text">La solicitud de participación de forma remota no implica que esta se acepte de forma automática. El congreso es presencial de forma obligatoria para las personas que participen con alguna propuesta, pero de haber una razón considerable podríamos aceptar charlas en remoto de forma excepcional (ejemplo: residir actualmente en otro país).</span> </div>';

let submit = '<div class="form-group text-center"><button name="submit" type="submit" class="btn btn-dark">Enviar</button></div>';

let target = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="target">Público objetivo:</label></strong> <div id="target-counter-tag" class="tag is-success"><span id="target-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="target" cols="40" rows="5" maxlength="500" style="resize:none; overflow-y: scroll;" aria-describedby="target-help-block" onkeyup="counterDisplay(this, \'target\', false)" class="form-control"></textarea><span id="target-help-block" class="form-text text-muted list-forms-text">¿A quién crees que podría resultarle interesante este tema (utilidad, campos relacionados, posibilidades...)?</span> </div>';

let target_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="target">Público objetivo:</label> </strong> <div id="target-counter-tag" class="tag is-success"> <span id="target-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000&nbsp;</div> <textarea name="target" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="target-help-block" onkeyup="counterDisplay(this, \'target\', false)" class="form-control"> </textarea> <span id="target-help-block" class="form-text text-muted list-forms-text">¿A quién crees que podría resultarle interesante este tema (utilidad, campos relacionados, posibilidades...)?</span> </div>';

let target_devrooms = '<div class="form-group"> <strong> <label for="target">Público objetivo (*):</label> </strong> <div id="target-counter-tag" class="tag is-danger"> <span id="target-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;1000</div> <textarea name="target" cols="40" rows="5" maxlength="1000" style="resize:none; overflow-y: scroll;" aria-describedby="target-help-block" onkeyup="counterDisplay(this, \'target\', true)" required="required" class="form-control"> </textarea> <span id="target-help-block" class="form-text text-muted">¿A quién crees que podría resultarle interesante este tema (utilidad, campos relacionados, posibilidades...)?</span> </div>';

let title = '<div class="form-group" style="margin-top: 1rem; margin-bottom: 1rem;"> <strong><label class="label-forms-text" for="title">Título (*):</label></strong> <div id="title-counter-tag" class="tag is-danger"><span id="title-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="title" type="text" maxlength="150" onkeyup="counterDisplay(this, \'title\', true)" required="required" class="form-control"> </div>';

let title_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="title">Título (*):</label> </strong> <div id="title-counter-tag" class="tag is-danger"> <span id="title-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="title" type="text" maxlength="150" onkeyup="counterDisplay(this, \'title\', true)" required="required" class="form-control"> </div>';

let twitter = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="twitter">Twitter:</label></strong> <div id="twitter-counter-tag" class="tag is-success"><span id="twitter-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;16&nbsp;</div><input name="twitter" type="text" maxlength="16" pattern="^@?[A-Za-z0-9_]{1,15}$" onkeyup="counterDisplay(this, \'twitter\', false)" class="form-control"><span id="mastodon-help-block" class="form-text text-muted list-forms-text">Indica solo tu username.</span> </div>';

let twitter_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="twitter">Twitter:</label> </strong> <div id="twitter-counter-tag" class="tag is-success"> <span id="twitter-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;16&nbsp;</div> <input name="twitter" type="text" maxlength="16" pattern="^@?[A-Za-z0-9_]{1,15}$" onkeyup="counterDisplay(this, \'twitter\', false)" class="form-control"> <span id="mastodon-help-block" class="form-text text-muted list-forms-text">Indica solo tu nombre en Twitter.</span> </div>';

let twitter_devrooms = '<div class="form-group"> <strong> <label for="twitter">Twitter:</label> </strong> <div id="twitter-counter-tag" class="tag is-success"> <span id="twitter-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150</div> <input name="twitter" type="text" maxlength="16" pattern="^@?[A-Za-z0-9_]{1,15}$" onkeyup="counterDisplay(this, \'twitter\', false)" class="form-control"> <span id="twitter-help-block" class="form-text text-muted">Indicad solo el nombre en Twitter.</span> </div>';

let url_poster = '<div class="form-group" style="margin-bottom: 1rem;"><strong><label class="label-forms-text" for="url">Enlace a al póster en <a href="https://commons.wikimedia.org/wiki/Special:UploadWizard?uselang=es" target="_blank">Wikimedia Commons</a> (*):</label></strong> <div id="url-counter-tag" class="tag is-danger"><span id="url-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;250&nbsp;</div><input name="url" type="text" maxlength="250" onkeyup="counterDisplay(this, \'url\', true)" required="required" class="form-control"><span id="url-help-block" class="form-text text-muted list-forms-text">Recuerda que la obra tiene que tener licencia <a style="color: #007bff !important;" href="https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES" target="_blank">Creative Commons BY-SA 4.0</a>. Es necesario una cuenta en Wikimedia Commons; si no tienes puedes crearla desde <a style="color: #007bff !important;" href="https://commons.wikimedia.org/w/index.php?title=Special:CreateAccount&uselang=es&returnto=Special%3AUploadWizard&returntoquery=uselang%3Des" target="_blank">aquí</a>.</span> </div>';

let url = '<div class="form-group" style="margin-bottom: 1rem;"><strong><label class="label-forms-text" for="url">Enlace a la obra en <a href="https://commons.wikimedia.org/wiki/Special:UploadWizard?uselang=es" target="_blank">Wikimedia Commons</a> (*):</label></strong> <div id="url-counter-tag" class="tag is-danger"><span id="url-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;250&nbsp;</div><input name="url" type="text" maxlength="250" onkeyup="counterDisplay(this, \'url\', true)" required="required" class="form-control"><span id="url-help-block" class="form-text text-muted list-forms-text">Recuerda que la obra tiene que tener licencia <a style="color: #007bff !important;" href="https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES" target="_blank">Creative Commons BY-SA 4.0</a>. Es necesario una cuenta en Wikimedia Commons; si no tienes puedes crearla desde <a style="color: #007bff !important;" href="https://commons.wikimedia.org/w/index.php?title=Special:CreateAccount&uselang=es&returnto=Special%3AUploadWizard&returntoquery=uselang%3Des" target="_blank">aquí</a>.</span> </div>';

let web = '<div class="form-group" style="padding-bottom: 1em"><strong><label class="label-forms-text" for="web">Web personal:</label></strong> <div id="web-counter-tag" class="tag is-success"><span id="web-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="web" type="url" maxlength="150" aria-describedby="web-help-block" onkeyup="counterDisplay(this, \'web\', false)" class="form-control"><span id="web-help-block" class="form-text text-muted list-forms-text">Por si tienes una web personal que quieras compartir.</span> </div>';

let web_proposals = '<div class="form-group"> <strong> <label class="label-forms-text" for="web">Web personal:</label> </strong> <div id="web-counter-tag" class="tag is-success"> <span id="web-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div> <input name="web" type="url" maxlength="150" aria-describedby="web-help-block" onkeyup="counterDisplay(this, \'web\', false)" class="form-control"> <span id="web-help-block" class="form-text text-muted list-forms-text">Por si tienes una web personal que quieras compartir.</span> </div>';

let web_devrooms = '<div class="form-group"> <strong> <label for="web">Web de la comunidad (*):</label> </strong> <div id="web-counter-tag" class="tag is-danger"> <span id="web-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150</div> <input name="web" type="url" maxlength="150" aria-describedby="web-help-block" onkeyup="counterDisplay(this, \'web\', true)" required="required" class="form-control"> <span id="web-help-block" class="form-text text-muted">Página con información sobre el grupo que propone organizar la sala.</span> </div>';

var elements = {
  abstract: abstract,
  abstract_proposals: abstract_proposals,
  accept_coc: accept_coc,
  accept_coc_proposals: accept_coc_proposals,
  accept_coc_devrooms: accept_coc_devrooms,
  bio: bio,
  bio_proposals: bio_proposals,
  bio_req: bio_req,
  call: call,
  comments: comments,
  comments_proposals: comments_proposals,
  comments_devrooms: comments_devrooms,
  community_description_devrooms: community_description_devrooms,
  community_name_devrooms: community_name_devrooms,
  contact_devrooms: contact_devrooms,
  description: description,
  description_proposals: description_proposals,
  description_devrooms: description_devrooms,
  disclaimer_venue: disclaimer_venue,
  email: email,
  email_proposals: email_proposals,
  email_devrooms: email_devrooms,
  format: format,
  format_proposals: format_proposals,
  format_devrooms: format_devrooms,
  format_miscs: format_miscs,
  format_venue: format_venue,
  gitlab: gitlab,
  gitlab_proposals: gitlab_proposals,
  gitlab_devrooms: gitlab_devrooms,
  grants: grants,
  mastodon: mastodon,
  mastodon_proposals: mastodon_proposals,
  mastodon_devrooms: mastodon_devrooms,
  name: name,
  name_proposals: name_proposals,
  name_venue: name_venue,
  portfolio: portfolio,
  project_proposals: project_proposals,
  project: project,
  remote: remote,
  submit: submit,
  target: target,
  target_proposals: target_proposals,
  target_devrooms: target_devrooms,
  title: title,
  title_proposals: title_proposals,
  twitter: twitter,
  twitter_proposals: twitter_proposals,
  twitter_devrooms: twitter_devrooms,
  url_poster: url_poster,
  url: url,
  web: web,
  web_proposals: web_proposals,
  web_devrooms: web_devrooms
};

module.exports = {
  elements
};
