"use strict";

module.exports = {
  app_name: "eslibre_c4p",
  event: "esLibre",
  event_name: "esLibre 2024: envío de propuestas",
  year: "2024",
  max_size: 20971520,
  production: {
    dialect: "sqlite",
    storage: "./db.production.2024.sqlite"
  },
  development: {
    dialect: "sqlite",
    storage: "./db.development.2024.sqlite"
  },
  test: {
    dialect: "sqlite",
    storage: ":memory:"
  },
  staging: {
    dialect: "mysql",
    host: "localhost",
    database: process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
  },
  auth: {
    enabled: true,
    user: process.env.LOGIN_USER,
    password: process.env.LOGIN_PASSWORD
  },
  gitlab: {
    token: process.env.GITLAB_TOKEN
  },
  twitter: {
    consumer_key: process.env.TWITTER_API_KEY,
    consumer_secret: process.env.TWITTER_API_SECRET_KEY,
    access_token_key: process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
  },
  email: {
    name: "esLibre (Tux)",
    user: "tux@eslib.re",
    replyto: 'hola@eslib.re',
    smtp: "ruido",
    server: "interferencias.tech",
    password: process.env.MAIL_PASSWORD,
    bcc: "Interferencias (Info) <info@interferencias.tech>",
    organization: "esLibre"
  },
  type: {
    proposal: 0,
    talk: 1,
    workshop: 2,
    poster: 3,
    art: 4,
    misc: 5,
    devroom: 6,
    table: 7,
    venue: 8,
    grants: 9
  },
  mode: {
    debug: 0,
    production: 1
  },
  language: {
    spanish: 0,
    english: 1,
    galician: 2
  },
  external_url_ok_es: "https://eslib.re/2024/recibida/",
  external_url_error_es: "https://eslib.re/2024/error/",
  external_url_ok_en: "https://eslib.re/2024/received/",
  external_url_error_en: "https://eslib.re/2024/failed/",
  external_url_ok_gl: "https://eslib.re/2024/recibida-gl/",
  external_url_error_gl: "https://eslib.re/2024/error-gl/"
};
